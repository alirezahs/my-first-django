from django.db import models

# Create your models here.
class User(models.Model):
    user_id = models.AutoField(unique = True, primary_key = True)
    fname = models.CharField(max_length = 264)
    lname = models.CharField(max_length = 264)
    phone = models.CharField(max_length = 264)
    age = models.IntegerField()
    email = models.EmailField()
    url = models.URLField()

class Post(models.Model):
    post_id = models.AutoField(unique= True, primary_key = True)
    #type = models.CharField(choices=[(Published, "published"), (Draft, "Draft")] )
    date = models.DateField(auto_now=True)
    title = models.CharField(max_length= 264)
    content = models.TextField()
