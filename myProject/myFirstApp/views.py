from django.shortcuts import render
from django.http import HttpResponse
from myFirstApp.models import User, Post
from . import forms
# Create your views here.

def index(request):
    my_dict = {"myVar":"Hello World !"}
    return render(request, "index.html", context = my_dict)

def help(request):
    return render(request, "help.html")

def users(request):
    user_list = User.objects.order_by("fname")
    dict = {"user_list" : user_list}
    return render(request, "index.html", context = dict)

def login(request):
    form = forms.Login()
    if request.method == 'POST':
        form = forms.Login(request.POST)
        if form.is_valid():
            print(form.cleaned_data['username'])
            print(form.cleaned_data['password'])
    return render(request, "login.html", {"login_form" : form})

def register(request):
    form = forms.RegisterForm()
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)

        if form.is_valid():
            form.save(commit = True)
            return index(request)
    return render(request, "register.html", {"register_form": form})