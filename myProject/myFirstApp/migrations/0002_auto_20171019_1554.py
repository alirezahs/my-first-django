# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-10-19 12:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myFirstApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.IntegerField(primary_key=True, serialize=False, unique=True)),
                ('fname', models.CharField(max_length=264)),
                ('lname', models.CharField(max_length=264)),
                ('phone', models.CharField(max_length=264)),
                ('age', models.IntegerField()),
                ('email', models.EmailField(max_length=254)),
                ('url', models.URLField()),
            ],
        ),
        migrations.RenameModel(
            old_name='posts',
            new_name='Post',
        ),
        migrations.DeleteModel(
            name='Users',
        ),
    ]
