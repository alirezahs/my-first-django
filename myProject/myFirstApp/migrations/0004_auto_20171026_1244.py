# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-26 09:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myFirstApp', '0003_auto_20171019_1604'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='fname',
            new_name='first_name',
        ),
    ]
