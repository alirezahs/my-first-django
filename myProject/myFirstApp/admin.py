from django.contrib import admin
from myFirstApp.models import User, Post
# Register your models here.
admin.site.register(User)
admin.site.register(Post)
