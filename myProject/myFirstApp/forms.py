from django import forms
from myFirstApp.models import User

class Login(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget = forms.PasswordInput)

class RegisterForm(forms.ModelForm):
    class Meta():
        model = User
        fields = "__all__"
        labels = {
            "fname" : "First Name",
            "lname" : "Last Name"
        }